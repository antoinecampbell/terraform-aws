provider "aws" {
  region = "${var.region}"
}

module "consul" {
  source = "hashicorp/consul/aws"

  num_servers = "1"
}