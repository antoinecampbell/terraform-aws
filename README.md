# Terraform - AWS

## Commands
Display changes that will be made
```bash
terraform plan
```

Apply changes
```bash
terraform apply
```

Remove infrastructure
```bash
terraform destroy
```