# Create the API gateway
resource "aws_api_gateway_rest_api" "example" {
  name = "ServerlessExample"
  description = "Terraform Serverless Application Example"
}

# Create node lambda resource
resource "aws_api_gateway_resource" "proxy_node" {
  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  parent_id = "${aws_api_gateway_rest_api.example.root_resource_id}"
  path_part = "node"
}

# Create java lambda resource
resource "aws_api_gateway_resource" "proxy_java" {
  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  parent_id = "${aws_api_gateway_rest_api.example.root_resource_id}"
  path_part = "java"
}

# Create node lambda method
resource "aws_api_gateway_method" "proxy_node" {
  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  resource_id = "${aws_api_gateway_resource.proxy_node.id}"
  http_method = "GET"
  authorization = "NONE"
}

# Create java lambda method
resource "aws_api_gateway_method" "proxy_java" {
  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  resource_id = "${aws_api_gateway_resource.proxy_java.id}"
  http_method = "GET"
  authorization = "NONE"
}

# Create node lambda integration request
resource "aws_api_gateway_integration" "lambda_node" {
  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  resource_id = "${aws_api_gateway_method.proxy_node.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_node.http_method}"

  integration_http_method = "POST" # Must be post for a lambda
  type = "AWS_PROXY"
  uri = "${aws_lambda_function.node.invoke_arn}"
}

# Create java lambda integration request
resource "aws_api_gateway_integration" "lambda_java" {
  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  resource_id = "${aws_api_gateway_method.proxy_java.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_java.http_method}"

  integration_http_method = "POST" # Must be post for a lambda
  type = "AWS_PROXY"
  uri = "${aws_lambda_function.java.invoke_arn}"
}

# API Gateway stage
resource "aws_api_gateway_deployment" "example" {
  depends_on = [
    "aws_api_gateway_integration.lambda_node",
    "aws_api_gateway_integration.lambda_java"
  ]

  rest_api_id = "${aws_api_gateway_rest_api.example.id}"
  stage_name = "test"
}

# Allow API Gateway to call the node lambda
resource "aws_lambda_permission" "apigw" {
  statement_id = "AllowAPIGatewayInvoke"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.node.function_name}"
  principal = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.example.execution_arn}/*/*/*"
}

# Allow API Gateway to call the java lambda
resource "aws_lambda_permission" "apigw_java" {
  statement_id = "AllowAPIGatewayInvoke"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.java.function_name}"
  principal = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.example.execution_arn}/*/*/*"
}

# URL used to invoke the API gateway
output "base_url" {
  value = "${aws_api_gateway_deployment.example.invoke_url}"
}

