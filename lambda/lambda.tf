provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "example" {
  bucket = "terraform-antoine-test-bucket"
  acl = "private"
}

resource "aws_s3_bucket_object" "node_zip" {
  bucket = "${aws_s3_bucket.example.id}"
  key    = "example.zip"
  source = "example.zip"
  etag   = "${md5(file("example.zip"))}"
}

resource "aws_s3_bucket_object" "java_zip" {
  bucket = "${aws_s3_bucket.example.id}"
  key    = "app.zip"
  source = "app.zip"
  etag   = "${md5(file("app.zip"))}"
}

resource "aws_lambda_function" "java" {
  function_name = "ServerlessExample-Java"

  # The bucket name as created earlier with "aws s3api create-bucket"
  s3_bucket = "${aws_s3_bucket.example.id}"
  s3_key = "app.zip"

  # "main" is the filename within the zip file (main.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "helloworld.App::handleRequest"
  runtime = "java8"

  role = "${aws_iam_role.lambda_exec.arn}"
}

resource "aws_lambda_function" "node" {
  function_name = "ServerlessExample-Node"

  # The bucket name as created earlier with "aws s3api create-bucket"
  s3_bucket = "${aws_s3_bucket.example.id}"
  s3_key = "example.zip"

  # "main" is the filename within the zip file (main.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "main.handler"
  runtime = "nodejs8.10"

  role = "${aws_iam_role.lambda_exec.arn}"
}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "lambda_exec" {
  name = "serverless_example_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}